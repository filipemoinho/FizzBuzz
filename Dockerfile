FROM fedora:27
RUN dnf install -y java-1.8.0-openjdk.x86_64
ADD ./target/fizzbuzz-0.0.1-SNAPSHOT.jar /fizzbuzz-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/fizzbuzz-0.0.1-SNAPSHOT.jar"]
