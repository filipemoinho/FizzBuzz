package com.moinho.fizzbuzz;

public class Fizzbuzz {
	
	public String validaFizzBuzz (int valor) {
		String textoFim = "";
		String texto = "";
		
		for (int i = 1; i<=valor ; i++) {

			texto = "";
			texto = restoPor3(i);
			texto += restoPor5(i);
			if (texto.equals("")) {
				texto += Integer.toString(i);
			}
			texto += " ";
			
			
			textoFim += texto;
		}
		
		System.out.println(textoFim);
		return textoFim.trim();

	}
	
	public String restoPor3(int numero) {
		if (numero % 3 == 0) {
			return "Fizz";
		}
		return "";
	}
	
	public String restoPor5(int numero) {
		if (numero % 5 == 0) {
			return "Buzz";
		}
		return "";
	}

}
