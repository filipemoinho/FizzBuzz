package com.moinho.fizzbuzz;

public class App 
{
    public static void main( String[] args )
    {
        Fizzbuzz fizzbuzz = new Fizzbuzz();
        fizzbuzz.validaFizzBuzz(2);
        fizzbuzz.validaFizzBuzz(3);
        fizzbuzz.validaFizzBuzz(5);
        fizzbuzz.validaFizzBuzz(15);
    }
}
