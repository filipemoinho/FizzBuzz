package com.moinho.fizzbuzz;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FizzbuzzTest {

	@Test
	public void testeBuzzValida1() {
		Fizzbuzz fizzBuzz = new Fizzbuzz();
		String retorno = fizzBuzz.validaFizzBuzz(1);
		assertEquals("1", retorno);	
	}

	@Test
	public void testeBuzzValida2() {
		Fizzbuzz fizzBuzz = new Fizzbuzz();
		String retorno = fizzBuzz.validaFizzBuzz(2);
		assertEquals("1 2", retorno);	
	}
	
	@Test
	public void testeBuzzValida3() {
		Fizzbuzz fizzBuzz = new Fizzbuzz();
		String retorno = fizzBuzz.validaFizzBuzz(3);
		assertEquals("1 2 Fizz", retorno);	
	}
	
	@Test
	public void testeBuzzValida4() {
		Fizzbuzz fizzBuzz = new Fizzbuzz();
		String retorno = fizzBuzz.validaFizzBuzz(4);
		assertEquals("1 2 Fizz 4", retorno);	
	}
	
	@Test
	public void testeBuzzValida5() {
		Fizzbuzz fizzBuzz = new Fizzbuzz();
		String retorno = fizzBuzz.validaFizzBuzz(5);
		assertEquals("1 2 Fizz 4 Buzz", retorno);	
	}
	
	@Test
	public void testeBuzzValida15() {
		Fizzbuzz fizzBuzz = new Fizzbuzz();
		String retorno = fizzBuzz.validaFizzBuzz(15);
		assertEquals("1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz", retorno);	
	}
	
}
